﻿using BankAccount.Application.Account.Deposit;
using BankAccount.Application.Account.Queries;
using BankAccount.Application.Account.Withdraw;
using BankAccount.Application.Interface;
using BankAccount.WebApi.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BankAccount.WebApi.Controllers.Account
{
    [Route("api/[controller]")]
    public class AccountsController : MainController
    {
        private readonly IWithdrawService _withdrawService;
        private readonly ICloseAccountService _closeAccountService;
        private readonly IDepositService _depositService;
        private readonly IGetBankAccountDetailQuery _bankAccountDetailQuery;

        public AccountsController(
            IWithdrawService withdrawService,
            ICloseAccountService closeAccountService,
            IDepositService depositService,
            IGetBankAccountDetailQuery bankAccountDetailQuery)
        {
            _withdrawService = withdrawService;
            _closeAccountService = closeAccountService;
            _depositService = depositService;
            _bankAccountDetailQuery = bankAccountDetailQuery;
        }


        [HttpGet("{accountId}", Name = "GetAccount")]
        public async Task<IActionResult> Get(Guid accountId)
        {
            var account = await _bankAccountDetailQuery.GetAccount(accountId);

            List<OperationModel> transactions = new List<OperationModel>();

            foreach (var item in account.ListOperations)
            {
                var transaction = new OperationModel(
                    item.Amount,
                    item.TransactionDate);

                transactions.Add(transaction);
            }

            return new ObjectResult(new AccountDetailsModel(
                account.AccountId,
                account.CurrentBalance,
                transactions));
        }


        [HttpPatch("Deposit")]
        public async Task<IActionResult> Deposit([FromBody] DepositRequest request)
        {
            DepositResult depositResult = await _depositService.Execute(
                request.AccountId,
                request.Amount);

            if (depositResult == null)
            {
                return new NoContentResult();
            }

            DepositModel model = new DepositModel(
                depositResult.OperationData.Amount,
                depositResult.OperationData.TransactionDate
            );

            return new ObjectResult(model);
        }

        [HttpPost("Withdraw")]
        public async Task<IActionResult> Withdraw([FromBody] WithdrawRequest request)
        {
            WithdrawResult depositResult = await _withdrawService.Execute(
                request.AccountId,
                request.Amount);

            if (depositResult == null)
            {
                return new NoContentResult();
            }

            WithdrawModel model = new WithdrawModel(
                depositResult.OperationData.Amount,
                depositResult.OperationData.TransactionDate

            );

            return new ObjectResult(model);
        }


        [HttpDelete("{accountId}")]
        public async Task<IActionResult> Close(Guid accountId)
        {
            Guid closeResult = await _closeAccountService.Execute(accountId);

            if (closeResult == Guid.Empty)
            {
                return new NoContentResult();
            }

            return Ok();
        }
    }
}
