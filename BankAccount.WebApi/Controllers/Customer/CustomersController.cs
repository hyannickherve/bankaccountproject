﻿using BankAccount.Application.Account.Register;
using BankAccount.Application.Customer.Queries;
using BankAccount.Application.Interface;
using BankAccount.Domain.Customer;
using BankAccount.WebApi.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BankAccount.WebApi.Controllers.Customer
{
    [Route("api/[controller]")]
    public class CustomersController : MainController
    {
        private readonly IRegisterAccountService _registerAccountService;
        private readonly IGetCustomerDetailQuery _customerDetailQuery;

        public CustomersController(IRegisterAccountService registerAccountService)
        {
            _registerAccountService =
                registerAccountService ??
                throw new ArgumentNullException(nameof(registerAccountService));
        }


        [HttpGet("{customerId}", Name = "GetCustomer")]
        public async Task<IActionResult> GetCustomer(Guid customerId)
        {
            var customer = await _customerDetailQuery.GetCustomer(customerId);
            if (customer == null)
            {
                return new NoContentResult();
            }
            List<AccountDetailsModel> listResultsData = new List<AccountDetailsModel>();

            foreach (var account in customer.Accounts)
            {

                List<OperationModel> listoperations = new List<OperationModel>();

                foreach (var operation in account.ListOperations)
                {
                    listoperations.Add(new OperationModel(
                        operation.Montant,
                        operation.OperationDate));
                }

                listResultsData.Add(
                    new AccountDetailsModel(
                    account.AccountId,
                    account.CurrentBalance,
                    listoperations));
            }

            CustomerDetailsModel result = new CustomerDetailsModel();
            return new ObjectResult(result);
        }



        [HttpPost("register")]
        public async Task<IActionResult> Post([FromBody] RegistrationModel registratioModel)
        {
            ResgisterResult registerResult = await _registerAccountService.Execute(
                registratioModel.FirstName,
                registratioModel.LastName,
                registratioModel.Montant);

            var listOperations = new List<OperationModel>();

            foreach (var item in registerResult.Account.ListOperations)
            {
                var operation = new
                    OperationModel(
                    item.Amount,
                    item.OperationDate);

                listOperations.Add(operation);
            }

            AccountDetailsModel account = new(
           registerResult.Account.AccountId,
           registerResult.Account.CurrentBalance,
           listOperations);

            List<AccountDetailsModel> listAccounts = new()
            {
                account
            };

            CustomerModel model = new (
              registerResult.Customer.CustomerId,
              registerResult.Customer.FirstName,
              registerResult.Customer.LastName,
              registerResult
          );
            return CreatedAtRoute("GetCustomer", new { customerId = model.CustomerId }, model);
        }
    }
}
