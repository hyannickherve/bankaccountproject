﻿using System;

namespace BankAccount.WebApi.Models
{
    public sealed class OperationModel
    {
        public double Amount { get; }
        public DateTime TransactionDate { get; }
        public OperationModel(double amount,DateTime transactionDate)
        {
            Amount = amount;
            TransactionDate = transactionDate;
        }
    }
}
