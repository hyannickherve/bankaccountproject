﻿using System;

namespace BankAccount.WebApi.Models
{
    public sealed class WithdrawModel
    {
        public WithdrawModel(double amount, DateTime transactionDate)
        {
            Amount = amount;
            TransactionDate = transactionDate;
        }

        public double Amount { get; }
        public DateTime TransactionDate { get; }
    }
}
