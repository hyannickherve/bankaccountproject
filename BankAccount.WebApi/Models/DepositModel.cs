﻿using System;

namespace BankAccount.WebApi.Models
{
    public class DepositModel
    {
        public DepositModel(double amount, DateTime transactionDate)
        {
            Amount = amount;
            TransactionDate = transactionDate;
        }

        public double Amount { get; }
        public DateTime TransactionDate { get; }
    }
}
