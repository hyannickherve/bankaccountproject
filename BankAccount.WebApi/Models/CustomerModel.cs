﻿using BankAccount.Application.Account.Register;
using System;

namespace BankAccount.WebApi.Models
{
    public sealed class CustomerModel
    {
        public CustomerModel(Guid customerId, string firstName, string lastName, ResgisterResult registerResult)
        {
            CustomerId = customerId;
            FirstName = firstName;
            LastName = lastName;
            RegisterResult = registerResult;
        }

        public Guid CustomerId { get; }
        public string FirstName { get; }
        public string LastName { get; }
        public ResgisterResult RegisterResult { get; }
    }
}
