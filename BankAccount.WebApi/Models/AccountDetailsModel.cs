﻿using System;
using System.Collections.Generic;

namespace BankAccount.WebApi.Models
{
    public sealed class AccountDetailsModel
    {
        public AccountDetailsModel(Guid accountId, double currentBalance, List<OperationModel> listOperations)
        {
            AccountId = accountId;
            CurrentBalance = currentBalance;
            ListOperations = listOperations;
        }

        public Guid AccountId { get; }
        public double CurrentBalance { get; }
        public List<OperationModel> ListOperations { get; }
    }
}
