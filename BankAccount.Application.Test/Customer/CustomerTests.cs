﻿using BankAccount.Application.Account.Register;
using BankAccount.Application.Repositories;
using Moq;
using System;
using Xunit;

namespace BankAccount.Application.Customer
{
    public class CustomerTests
    {
        private readonly Mock<IAccountReadOnlyRepository> _accountReadOnlyRepositoryMock;
        private readonly Mock<IAccountWriteOnlyRepository> _accountWriteOnlyRepositoryMock;
        private readonly Mock<ICustomerReadOnlyRepository> _customerReadOnlyRepositoryMock;
        private readonly Mock<ICustomerWriteOnlyRepository> _customerWriteOnlyRepositoryMock;

        private readonly RegisterAccountService _registerProcessor;


        public CustomerTests()
        {
            _accountReadOnlyRepositoryMock = new Mock<IAccountReadOnlyRepository>();
            _accountWriteOnlyRepositoryMock = new Mock<IAccountWriteOnlyRepository>();
            _customerReadOnlyRepositoryMock = new Mock<ICustomerReadOnlyRepository>();
            _customerWriteOnlyRepositoryMock = new Mock<ICustomerWriteOnlyRepository>();

            _registerProcessor = new RegisterAccountService(
                _customerWriteOnlyRepositoryMock.Object,
                _accountWriteOnlyRepositoryMock.Object);
        }

        [Theory]
        //  [MemberData(nameof(GetData()))]
        [InlineData("firstname1", "lastname1", 3000)]
        [InlineData("firstname2", "lastname2", 450)]
        [InlineData("firstname3", "lastname3", 410)]
        public async void Customer_Account_Should_Be_Valid(string firstname, string lastname, double amount)
        {
            var _registerResult = await _registerProcessor.Execute(firstname, lastname, amount);


            Assert.Equal(firstname, _registerResult.Customer.FirstName);
            Assert.Equal(lastname, _registerResult.Customer.LastName);
            Assert.True(_registerResult.Account.AccountId != Guid.Empty);
        }

     
    }
}
