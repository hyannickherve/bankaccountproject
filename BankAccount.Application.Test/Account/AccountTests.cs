﻿using BankAccount.Application.Account.Register;
using BankAccount.Application.Interface;
using BankAccount.Application.Repositories;
using BankAccount.Domain.Common;
using Moq;
using System;
using Xunit;

namespace BankAccount.Application.Account
{
    public class AccountTests
    {
        private readonly RegisterAccountService _registerProcessor;
        private readonly Mock<IAccountReadOnlyRepository> _accountReadOnlyRepositoryMock;
        private readonly Mock<IAccountWriteOnlyRepository> _accountWriteOnlyRepositoryMock;


        public readonly Mock<ICustomerReadOnlyRepository> _customerReadOnlyRepository;
        public readonly Mock<ICustomerWriteOnlyRepository> _customerWriteOnlyRepository;




        public AccountTests()
        {
            _accountReadOnlyRepositoryMock = new Mock<IAccountReadOnlyRepository>();
            _accountWriteOnlyRepositoryMock = new Mock<IAccountWriteOnlyRepository>();
            _customerWriteOnlyRepository = new Mock<ICustomerWriteOnlyRepository>();
            _customerReadOnlyRepository = new Mock<ICustomerReadOnlyRepository>();

            _registerProcessor = new RegisterAccountService(_customerWriteOnlyRepository.Object, _accountWriteOnlyRepositoryMock.Object);
        }

        [Theory]
        [InlineData("firstName", "lastName", 1000)]
        [InlineData("firstName1", "lastName1", 5000)]
        public async void Register_User_With_Valid_Account(string firstName, string lastName, double montant)
        {

            //Arrange
            var _registerResult = await _registerProcessor.Execute(firstName, lastName, montant);

            //Assert
            Assert.Equal(firstName, _registerResult.Customer.FirstName);
            Assert.Equal(lastName, _registerResult.Customer.LastName);
            Assert.NotEqual(Guid.Empty, _registerResult.Customer.CustomerId);

        }


        [Theory]
        [InlineData(100)]
        public void Account_With_Credits_Can_Not_Be_Allow_Closed(double amount)
        {
            var account = new Domain.Account.BankAccount(Guid.NewGuid());
            account.Deposit(amount);

            Assert.Throws<BankAccountCloseException>(
                () => account.Close());
        }

    }
}
