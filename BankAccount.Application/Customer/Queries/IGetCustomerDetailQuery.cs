﻿using BankAccount.Application.Results;
using System;
using System.Threading.Tasks;

namespace BankAccount.Application.Customer.Queries
{
    public interface IGetCustomerDetailQuery
    {
        Task<CustomerResult> GetCustomer(Guid id);
    }
}
