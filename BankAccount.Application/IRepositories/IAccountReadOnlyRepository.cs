﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankAccount.Application.Repositories
{
    public interface IAccountReadOnlyRepository
    {
        Task<Domain.Account.BankAccount> Get(Guid id);
    }
}
