﻿using System;
using System.Threading.Tasks;

namespace BankAccount.Application.Repositories
{
    public interface ICustomerReadOnlyRepository
    {
        Task<Domain.Customer.Customer> Get(Guid id);
    }
}
