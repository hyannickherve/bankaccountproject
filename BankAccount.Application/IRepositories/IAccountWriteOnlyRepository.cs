﻿using BankAccount.Domain.Common;
using System;
using System.Threading.Tasks;

namespace BankAccount.Application.Repositories
{
    public interface IAccountWriteOnlyRepository
    {
        Task Add(Domain.Account.BankAccount account, Credit credit);
        Task Update(Domain.Account.BankAccount account, Credit credit);
        Task Update(Domain.Account.BankAccount account, Debit debit);
       // Task Delete(Domain.Account.BankAccount account);
        Task<Guid> Delete(Domain.Account.BankAccount account);
    }
}
