﻿using BankAccount.Domain.Customer;
using System;
using System.Threading.Tasks;
namespace BankAccount.Application.Repositories
{
    public interface ICustomerWriteOnlyRepository
    {

        Task Add(Domain.Customer.Customer customer);
        Task Update(Domain.Customer.Customer customer);

    }
}
