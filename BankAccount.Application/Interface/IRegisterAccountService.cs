﻿using BankAccount.Application.Account.Register;

namespace BankAccount.Application.Interface
{
    public interface IRegisterAccountService
    {
        System.Threading.Tasks.Task<ResgisterResult> Execute(string firstName, string lastName, double montant);
    }
}