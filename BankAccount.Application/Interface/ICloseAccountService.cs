﻿using System;
using System.Threading.Tasks;

namespace BankAccount.Application.Interface
{
    public interface ICloseAccountService
    {
        Task<Guid> Execute(Guid accountId);
    }
}