﻿using BankAccount.Application.Account.Deposit;
using BankAccount.Domain.Common;
using System;
using System.Threading.Tasks;

namespace BankAccount.Application.Interface
{
    public interface IDepositService
    {
        Task<DepositResult> Execute(Guid accountId, Amount amount);

    }
}
