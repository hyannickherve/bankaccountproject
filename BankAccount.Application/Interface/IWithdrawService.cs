﻿using BankAccount.Application.Account.Withdraw;
using BankAccount.Domain.Common;
using System;
using System.Threading.Tasks;

namespace BankAccount.Application.Interface
{
    public interface IWithdrawService
    {
        Task<WithdrawResult> Execute(Guid accountId, Amount amount);
    }
}
