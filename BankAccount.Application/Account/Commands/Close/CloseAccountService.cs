﻿using BankAccount.Application.Interface;
using BankAccount.Application.Repositories;
using BankAccount.Domain.Common;
using System;
using System.Threading.Tasks;

namespace BankAccount.Application.Account.Close
{
    public class CloseAccountService : ICloseAccountService
    {
        private readonly IAccountReadOnlyRepository _accountReadOnlyRepository;
        private readonly IAccountWriteOnlyRepository _accountWriteOnlyRepository;

        public CloseAccountService(IAccountReadOnlyRepository accountReadOnlyRepository,
            IAccountWriteOnlyRepository accountWriteOnlyRepository)
        {
            _accountReadOnlyRepository = accountReadOnlyRepository;
            _accountWriteOnlyRepository = accountWriteOnlyRepository;
        }

        public async Task<Guid> Execute(Guid accountId)
        {
            Domain.Account.BankAccount account = await _accountReadOnlyRepository.Get(accountId);
            if (account == null)
                throw new BankAccountCloseException($"The account {accountId} does not exists or is already closed.");

            account.Close();

            await _accountWriteOnlyRepository.Delete(account);

            return account.IdCustomer;
        }
    }
}
