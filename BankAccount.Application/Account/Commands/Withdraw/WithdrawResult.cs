﻿using BankAccount.Application.Results;
using BankAccount.Domain.Common;

namespace BankAccount.Application.Account.Withdraw
{
    public class WithdrawResult
    {
        public WithdrawResult(Debit debit, Amount amount)
        {
            Debit = debit;
            Amount = amount;
        }

        public OperationResult OperationData { get; set; }

        public Debit Debit { get; }
        public Amount Amount { get; }
    }
}