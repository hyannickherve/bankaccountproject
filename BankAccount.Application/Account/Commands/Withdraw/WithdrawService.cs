﻿using BankAccount.Application.Interface;
using BankAccount.Application.Repositories;
using BankAccount.Domain.Common;
using System;
using System.Threading.Tasks;

namespace BankAccount.Application.Account.Withdraw
{
    public class WithdrawService : IWithdrawService
    {
        private readonly IAccountReadOnlyRepository _accountReadOnlyRepository;
        private readonly IAccountWriteOnlyRepository _accountWriteOnlyRepository;

        public WithdrawService(IAccountReadOnlyRepository accountReadOnlyRepository,
            IAccountWriteOnlyRepository accountWriteOnlyRepository)
        {
            _accountReadOnlyRepository = accountReadOnlyRepository;
            _accountWriteOnlyRepository = accountWriteOnlyRepository;
        }

        public async Task<WithdrawResult> Execute(Guid accountId, Amount amount)
        {

            Domain.Account.BankAccount account = await _accountReadOnlyRepository.Get(accountId);
            if (account == null)
                throw new AccountNotFoundException($"The account {accountId} does not exists or is already closed.");

            account.WithDraw(amount);
            Debit debit = (Debit)account.GetLastOperation();

            await _accountWriteOnlyRepository.Update(account, debit);

            WithdrawResult result = new WithdrawResult(
                debit,
                account.GetBalance()
            );

            return result;
        }
    }
}
