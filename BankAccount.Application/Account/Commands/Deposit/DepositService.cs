﻿using BankAccount.Application.Interface;
using BankAccount.Application.Repositories;
using BankAccount.Domain.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankAccount.Application.Account.Deposit
{
    public sealed class DepositService : IDepositService
    {

        private readonly IAccountReadOnlyRepository _accountReadOnlyRepository;
        private readonly IAccountWriteOnlyRepository _accountWriteOnlyRepository;

        public DepositService(IAccountReadOnlyRepository accountReadOnlyRepository,
            IAccountWriteOnlyRepository accountWriteOnlyRepository)
        {
            _accountReadOnlyRepository = accountReadOnlyRepository;
            _accountWriteOnlyRepository = accountWriteOnlyRepository;
        }

        public async Task<DepositResult> Execute(Guid accountId, Amount amount)
        {
            Domain.Account.BankAccount account = await _accountReadOnlyRepository.Get(accountId);
            if (account == null)
                throw new BankAccountCloseException($"The account {accountId} does not exists or is already closed.");

            account.Deposit(amount);
            Credit credit = (Credit)account.GetLastOperation();

            await _accountWriteOnlyRepository.Update(
                account,
                credit);

            DepositResult result = new DepositResult(
                credit,
                account.GetBalance());
            return result;
        }
    }
}
