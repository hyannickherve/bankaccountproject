﻿using BankAccount.Application.Results;
using BankAccount.Domain.Common;

namespace BankAccount.Application.Account.Deposit
{
    public class DepositResult
    {
        private Credit credit;
        private Amount amount;

        public DepositResult(Credit credit, Amount amount)
        {
            this.credit = credit;
            this.amount = amount;
        }

        public OperationResult OperationData { get; set; }
    }
}