﻿using BankAccount.Application.Results;
using BankAccount.Domain.Common;
using BankAccount.Domain.Customer;
using System.Collections.Generic;

namespace BankAccount.Application.Account.Register
{
    public class ResgisterResult
    {
        private Domain.Customer.Customer customer;
        private Domain.Account.BankAccount account;

        public ResgisterResult(Domain.Customer.Customer customer, Domain.Account.BankAccount account)
        {
            List<OperationResult> transactionResults = new List<OperationResult>();

            foreach (IOperation transaction in account.GetListOperations())
            {
                transactionResults.Add(
                    new OperationResult(
                        transaction.Montant,
                        transaction.OperationDate));
            }

            Account = new AccountResult(account.IdCustomer, account.GetBalance(), transactionResults);

            List<AccountResult> listAccount = new List<AccountResult>();
            listAccount.Add(Account);

            Customer = new CustomerResult(customer.IdCustomer, customer.FirstName, customer.LastName, listAccount);
        }

        public CustomerResult Customer { get; }
        public AccountResult Account { get; }
    }
}