﻿using BankAccount.Application.Interface;
using BankAccount.Application.Repositories;
using BankAccount.Domain.Common;
using BankAccount.Domain.Customer;
using System.Threading.Tasks;



namespace BankAccount.Application.Account.Register
{
    public class RegisterAccountService : IRegisterAccountService
    {
        private readonly ICustomerWriteOnlyRepository _customerWriteOnlyRepository;
        private readonly IAccountWriteOnlyRepository _accountWriteOnlyRepository;


        public RegisterAccountService(
           ICustomerWriteOnlyRepository customerWriteOnlyRepository,
           IAccountWriteOnlyRepository accountWriteOnlyRepository)
        {
            _customerWriteOnlyRepository = customerWriteOnlyRepository;
            _accountWriteOnlyRepository = accountWriteOnlyRepository;
        }


        public async Task<ResgisterResult> Execute(string firstName, string lastName, double montantDepot)
        {
            Domain.Customer.Customer customer = new Domain.Customer.Customer(firstName, lastName);

            var account = new Domain.Account.BankAccount(customer.IdCustomer);
            account.Deposit(montantDepot);
            Credit credit = account.GetLastOperation() as Credit;


            customer.Register(customer.IdCustomer);

            await _customerWriteOnlyRepository.Add(customer);
            await _accountWriteOnlyRepository.Add(account, credit);

            return new ResgisterResult(customer, account);
        }
    }
}