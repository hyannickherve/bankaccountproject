﻿using BankAccount.Application.Results;
using System;
using System.Threading.Tasks;

namespace BankAccount.Application.Account.Queries
{
    public interface IGetBankAccountDetailQuery
    {
        Task<AccountResult> GetAccount(Guid id);
    }
}
