﻿using BankAccount.Domain.Common;
using System;

namespace BankAccount.Application.Results
{
    public sealed class OperationResult 
    {
        public double Amount { get; }
        public DateTime TransactionDate { get; }
        public Amount Montant { get; }
        public DateTime OperationDate { get; }

        public OperationResult(
                      double amount,
            DateTime transactionDate)
        {
            Amount = amount;
            TransactionDate = transactionDate;
        }

        public OperationResult(Amount montant, DateTime operationDate)
        {
            Montant = montant;
            OperationDate = operationDate;
        }
    }
}