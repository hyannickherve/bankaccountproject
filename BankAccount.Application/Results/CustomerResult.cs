﻿using System;
using System.Collections.Generic;

namespace BankAccount.Application.Results
{
    public class CustomerResult
    {
        public Guid CustomerId { get; }

        public string FirstName { get; }
        public string LastName { get; }
        public IReadOnlyList<AccountResult> Accounts { get; }

        public CustomerResult(
            Guid customerId,

            string firstname,
            string lastname,
            List<AccountResult> accounts)
        {
            CustomerId = customerId;
            FirstName = firstname;
            LastName = lastname;
            Accounts = accounts;
        }
    }
}
