﻿using BankAccount.Domain.Common;
using System;
using System.Collections.Generic;

namespace BankAccount.Application.Results
{
    public sealed class AccountResult
    {
        public Guid AccountId { get; }
        public double CurrentBalance { get; }
        public List<OperationResult> ListOperations { get; }

        public AccountResult(
            Guid accountId,
            double currentBalance,
            List<OperationResult> transactions)
        {
            AccountId = accountId;
            CurrentBalance = currentBalance;
            ListOperations = transactions;
        }

        public AccountResult(Domain.Account.BankAccount account)
        {
            AccountId = account.IdCustomer;
            CurrentBalance = account.GetBalance();

            List<OperationResult> transactionResults = new List<OperationResult>();
            foreach (IOperation operation in account.GetListOperations())
            {
                OperationResult transactionResult =
                    new(
                        operation.Montant,
                        operation.OperationDate
                    );
                transactionResults.Add(transactionResult);
            }

            ListOperations = transactionResults;
        }
    }
}
