﻿using BankAccount.Domain.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace BankAccount.Domain.Account
{
    public class AccountTests
    {
     
        [Fact]
        public void New_Client_Should_Make_Deposit_in_Account()
        {
           //arrange
            Guid clientid = Guid.NewGuid();
            BankAccount account = new BankAccount(clientid);


            //Arrange
            Amount amount = new Amount(1000.0);
            account.Deposit(amount);


            Credit credit = account.GetLastOperation() as Credit;


            //Assert
            Assert.Equal(clientid, account.IdCustomer);
            Assert.Equal(new Amount(1000.0), credit.Montant);
        }

      


        [Fact]
        public void New_Account_Should_Have_10_Credits_Minimun()
        {
            //Arrange
            var IdCustomer = new Guid();

            Amount amount = new Amount(4.0);
            BankAccount account = new BankAccount
            {
                IdCustomer = IdCustomer,
            };

            //Act
            account.Deposit(amount);

            // Consultation consultation =  (Consultation).account.GetLastTransaction();

            //Assert
            //Assert.Equal(100,account.)

        }


       

        [Fact]
        public void ACustomerShouldBeAbleToSeeAllMyTransaction()
        {
            //Arrange 
            BankAccount account = new BankAccount(Guid.NewGuid());
            account.Deposit(200);
            account.WithDraw(100);


            //Act
            List<IOperation> listTransactions = account.GetListOperations()?.ToList();

            Assert.NotNull(listTransactions);
            Assert.True(listTransactions.Any());
            Assert.Equal(2, listTransactions.Count);
        }


        [Fact]
        public void Account_With_Credit_Or_Debit_Is_Not_Allow_To_Be_Close()
        {
            //act
            BankAccount account = new BankAccount(Guid.NewGuid());
            account.Deposit(300);

            var exceptionFirst = Assert.Throws<BankAccountCloseException>(
                () => account.Close());


        }
    }
}
