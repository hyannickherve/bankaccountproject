﻿using System;
using Xunit;

namespace BankAccount.Domain.Customer
{
    public class CustomerTests
    {
        [Fact]
        public void Customer_With_Empty_Values_Should_Not_Be_Created()
        {

            // Act and Assert 
            var exceptionlastName= Assert.Throws<ArgumentNullException>(
                    () => new Customer("firstName", string.Empty));

            var exceptionFirst = Assert.Throws<ArgumentNullException>(
                   () => new Customer(null, "lastName"));

            //Assert
            Assert.Equal("lastName", exceptionlastName.ParamName);
            Assert.Equal("firstName", exceptionFirst.ParamName);
        }



        [Fact]
        public void Customer_Should_Be_Registered_With_One_Account()
        {
            // Arrange
            Customer customer = new Customer("FirstName", "LastName");

            //Act
            //  var processor = new OperationRequest();
            //  RegistrationRequestResult registrationResult = processor.Register(customer.IdCustomer);
            customer.Register(customer.IdCustomer);

            //Assert
            Assert.NotNull(customer.Accounts);
            Assert.Single(customer.Accounts);
        }
    }
}
