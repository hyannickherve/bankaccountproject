﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankAccount.Infrastructure.DataAccess
{
    public interface IApplicationContext
    {

         DbSet<Entities.BankAccount> BankAccounts { get; set; }
         DbSet<Entities.Credit> Credits { get; set; }
         DbSet<Entities.Customer> Customers { get; set; }
         DbSet<Entities.Debit> Debits { get; set; }


        Task<int> SaveChangesAsync();

    }
}
