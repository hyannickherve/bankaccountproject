﻿using System;

namespace BankAccount.Infrastructure.DataAccess.Entities
{
    public class BankAccount
    {
        public Guid Id { get; set; }
        public Guid IdCustomer { get; set; }

    }
}
