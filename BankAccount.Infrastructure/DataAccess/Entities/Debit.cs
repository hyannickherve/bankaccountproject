﻿using System;

namespace BankAccount.Infrastructure.DataAccess.Entities
{
    public class Debit
    {
        public DateTime OperationDate { get; set; }
        public double Montant { get; set; }
        public Guid Guid { get; set; }
        public Guid AccountId { get;  set; }
    }
}
