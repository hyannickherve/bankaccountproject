﻿using System;

namespace BankAccount.Infrastructure.DataAccess.Entities
{
    public class Credit
    {
        public Guid guid { get; set; }

        public DateTime OperationDate { get; set; }

        public double Montant { get; set; }
        public Guid AccountId { get; set; }
    }
}
