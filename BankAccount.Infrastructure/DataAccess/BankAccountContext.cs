﻿using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace BankAccount.Infrastructure.DataAccess
{
    public class BankAccountContext : DbContext, IApplicationContext
    {
        public BankAccountContext(DbContextOptions<BankAccountContext> options) : base(options)
        {
            //Database.SetInitializer(new DatabaseInitializer());
        }

        public DbSet<Entities.BankAccount> BankAccounts { get; set; }
        public DbSet<Entities.Credit> Credits { get; set; }
        public DbSet<Entities.Customer> Customers { get; set; }
        public DbSet<Entities.Debit> Debits { get; set; }

        public async Task<int> SaveChangesAsync()
        {
            return await base.SaveChangesAsync();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder
               .Entity<Entities.BankAccount>()
               .ToTable("BankAccount");

            modelBuilder
              .Entity<Entities.Credit>()
              .ToTable("Credit");

            modelBuilder
              .Entity<Entities.Customer>()
              .ToTable("Customer");

            modelBuilder
              .Entity<Entities.Debit>()
              .ToTable("Debit");
        }
    }
}
