﻿using BankAccount.Application.Repositories;
using BankAccount.Domain.Customer;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace BankAccount.Infrastructure.DataAccess.Repositories
{
    public class CustomerRepository : ICustomerReadOnlyRepository, ICustomerWriteOnlyRepository
    {
        private readonly IApplicationContext _dbContext;

        public CustomerRepository(IApplicationContext dbContext)
        {
            _dbContext = dbContext;
        }



        public async Task Add(Customer customer)
        {
            await _dbContext.Customers.AddAsync(
                new Entities.Customer()
                {
                    Id = customer.IdCustomer,
                    FirstName = customer.FirstName,
                    LastName = customer.LastName,
                });

            await _dbContext.SaveChangesAsync();
        }

        public async Task<Customer> Get(Guid id)
        {
            Entities.Customer customer = await _dbContext.Customers
                 .FindAsync(id);

            var listAccounts = await _dbContext.BankAccounts
                .Where(a => a.IdCustomer == id)
                .Select(c => c.Id)
                .ToListAsync();
            //var customerResult =  new Customer(customer.FirstName, customer.LastName);
            //customerResult.

            return new Customer(customer.FirstName, customer.LastName);
        }

        public async Task Update(Customer customer)
        {
            var data = await _dbContext.Customers.FindAsync(customer.IdCustomer);

            if (data != null)
            {
                data.FirstName = customer.FirstName;
                data.LastName = customer.LastName;
                _dbContext.Customers.Update(data);

                await _dbContext.SaveChangesAsync();
            }
        }
    }
}
