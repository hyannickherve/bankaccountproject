﻿using BankAccount.Application.Repositories;
using BankAccount.Domain.Common;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace BankAccount.Infrastructure.DataAccess.Repositories
{
    public class AccountRepository : IAccountReadOnlyRepository, IAccountWriteOnlyRepository
    {

        private readonly IApplicationContext _dbContext;

        public AccountRepository(IApplicationContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task Add(Domain.Account.BankAccount account, Credit credit)
        {
            await _dbContext.BankAccounts.AddAsync(new Entities.BankAccount()
            {
                Id = account.guid,
                IdCustomer = account.IdCustomer
            });

            await _dbContext.Credits.AddAsync(new Entities.Credit()
            {
                guid = credit.guid,
                Montant = credit.Montant,
                OperationDate = credit.OperationDate
            });

            await _dbContext.SaveChangesAsync();
        }

        public async Task<Guid> Delete(Domain.Account.BankAccount account)
        {
            var result = _dbContext.BankAccounts.FirstOrDefault(a => a.Id == account.guid);
            if (result == null)
            {
                return default;
            }
            _dbContext.BankAccounts.Remove(result);
            await _dbContext.SaveChangesAsync();
            return result.Id;

        }

        public async Task<Domain.Account.BankAccount> Get(Guid id)
        {

            var account = await _dbContext
                              .BankAccounts
                              .FindAsync(id);

            var credit = await _dbContext
                 .Credits.Where(a => a.guid == id)
                 .ToListAsync();

            var debits = await _dbContext
               .Debits.Where(a => a.Guid == id)
               .ToListAsync();

            return default;


        }

        public async Task Update(Domain.Account.BankAccount account, Credit credit)
        {
            Entities.Credit data = new Entities.Credit
            {
               guid = credit.guid,
               Montant = credit.Montant,
               OperationDate = credit.OperationDate,
            };

            await _dbContext.Credits.AddAsync(data);
            await _dbContext.SaveChangesAsync();
        }

        public async Task Update(Domain.Account.BankAccount account, Debit debit)
        {
            Entities.Debit data = new Entities.Debit
            {
                Guid = debit.Guid,
                Montant = debit.Montant,
                OperationDate = debit.OperationDate
            };

            await _dbContext.Debits.AddAsync(data);
            await _dbContext.SaveChangesAsync();
        }
    }
}
