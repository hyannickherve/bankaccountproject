﻿using BankAccount.Application.Account.Queries;
using BankAccount.Application.Customer.Queries;
using BankAccount.Application.Results;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankAccount.Infrastructure.DataAccess.Queries
{
    public class GetCustomerDetailQuery : IGetCustomerDetailQuery
    {
        private readonly IApplicationContext _dbContext;
        private readonly IGetBankAccountDetailQuery _bankAccountDetailQuery;

        public GetCustomerDetailQuery(
            IApplicationContext dbContext, 
            IGetBankAccountDetailQuery bankAccountDetailQuery)
        {
            _dbContext = dbContext;
            _bankAccountDetailQuery = bankAccountDetailQuery;
        }

        public async Task<CustomerResult> GetCustomer(Guid id)
        {
            Entities.Customer customer = await _dbContext.Customers
              .FindAsync(id);

            if (customer == null)
            {
                return null;
            }

            var listAccounts = await _dbContext.BankAccounts
                .Where(a => a.IdCustomer == id)
                .ToListAsync();
              
            List<AccountResult> listresultDatas = new();

            foreach (var item in listAccounts)
            {
                AccountResult accountResult = await _bankAccountDetailQuery
                    .GetAccount(item.Id);
                listresultDatas.Add(accountResult);
            }


            CustomerResult customerResult = new CustomerResult(customer.Id
                , customer.FirstName, customer.LastName, listresultDatas);


            return await Task.FromResult(customerResult);
        }
    }
}
