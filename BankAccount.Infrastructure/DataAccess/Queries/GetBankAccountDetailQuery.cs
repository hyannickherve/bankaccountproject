﻿using BankAccount.Application.Account.Queries;
using BankAccount.Application.Results;
using BankAccount.Domain.Common;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BankAccount.Infrastructure.DataAccess.Queries
{
    public class GetBankAccountDetailQuery : IGetBankAccountDetailQuery
    {
        private readonly IApplicationContext _dbContext;

        public GetBankAccountDetailQuery(IApplicationContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<AccountResult> GetAccount(Guid id)
        {
            Entities.BankAccount account = await _dbContext.BankAccounts
                   .FirstAsync(b => b.Id == id);

            if (account == null)
            {
                return null;
            }

            //retrieve credits datas
            var credits = await _dbContext.Credits
                  .Where(a => a.AccountId == account.Id)
                  .ToListAsync();

            List<IOperation> listOperations = new();

            foreach (var item in credits)
            {
                listOperations.Add(new Credit(id, new Amount(item.Montant)));
            }

            //retrieve debits datas
            var debits = await _dbContext.Debits
                 .Where(a => a.AccountId == account.Id)
                 .ToListAsync();

            foreach (var item in debits)
            {
                listOperations.Add(new Debit(id, new Amount(item.Montant)));
            }

            var resultDatas = Domain.Account.BankAccount.
                     SetBankAccountDatas(account.Id, account.IdCustomer, listOperations);

            return new AccountResult(resultDatas);

        }
    }
}
