﻿using Autofac;
using BankAccount.Infrastructure.DataAccess;
using Microsoft.EntityFrameworkCore;

namespace BankAccount.Infrastructure.Modules
{
    public class Module : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            //base.Load(builder); 

            builder.RegisterType<BankAccountContext>()
                .As<IApplicationContext>()
                .InstancePerRequest();
        }
    }
}
