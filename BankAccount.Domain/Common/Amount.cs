﻿using System;

namespace BankAccount.Domain.Common
{
    public sealed class Amount
    {
        private double _montant;


        public Amount(double montant)
        {
            _montant = montant;
        }

        public override string ToString()
        {
            return _montant.ToString();
        }

        public static implicit operator double(Amount montant)
        {
            return montant._montant;
        }


        public static implicit operator Amount(double montant)
        {
            return new Amount(montant);
        }

        public static Amount operator +(Amount amount1, Amount amount2)
        {
            return new Amount(amount1._montant + amount2._montant);
        }

        public static Amount operator -(Amount amount1, Amount amount2)
        {
            return new Amount(amount1._montant - amount2._montant);
        }

        public static bool operator >(Amount mtt , Amount mtt1)
        {
            return mtt._montant > mtt1._montant;
        }


        public static bool operator <(Amount mtt, Amount mtt1)
        {
            return mtt._montant < mtt1._montant;
        }
    }
}