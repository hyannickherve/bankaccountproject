﻿using System;

namespace BankAccount.Domain.Common
{
    public interface IOperation
    {

        DateTime OperationDate { get; set; }
        Amount Montant { get;  set; }
    }
}
