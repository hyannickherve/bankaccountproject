﻿using System;

namespace BankAccount.Domain.Common
{
    public class Credit : IOperation , IEntityBase
    {
        public Guid guid { get;  set; }
        // private Amount montant;

        public Credit()
        {

        }
        public Credit(Guid guid, Amount montant)
        {
            this.guid = guid;
            Montant = montant;

        }

        public DateTime OperationDate { get; set; }

        // public Amount Montant { get;  set; }
        public Amount Montant { get; set; }
    }
}