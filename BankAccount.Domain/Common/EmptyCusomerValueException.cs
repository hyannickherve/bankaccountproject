﻿using System;
using System.Runtime.Serialization;

namespace BankAccount.Domain.Common
{
    [Serializable]
    public class EmptyCusomerValuesException : Exception
    {
        public EmptyCusomerValuesException()
        {
        }

        public EmptyCusomerValuesException(string message) : base(message)
        {
        }

        public EmptyCusomerValuesException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected EmptyCusomerValuesException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}