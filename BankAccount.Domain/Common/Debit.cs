﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankAccount.Domain.Common
{
    public class Debit : IOperation
    {
        public Debit(Guid guid, Amount montant)
        {
            Guid = guid;
            Montant = montant;
        }

        public DateTime OperationDate { get; set; }
        public Amount Montant { get; set; }
        public Guid Guid { get; set; }
    }
}
