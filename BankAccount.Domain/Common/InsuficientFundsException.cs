﻿using System;
using System.Runtime.Serialization;

namespace BankAccount.Domain.Common
{
   
    public sealed class InsuficientFundsException : Exception
    {
        public InsuficientFundsException(string message) : base(message)
        {
        }

    }
}