﻿using System;

namespace BankAccount.Domain.Common
{
    public class BankAccountCloseException : Exception
    {
        public BankAccountCloseException()
        {
        }

        public BankAccountCloseException(string message) : base(message)
        {
        }
    }
}