﻿using System;

namespace BankAccount.Domain.Customer
{
    public sealed class RegistrationModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public double Montant { get; set; }
      
    }
}