﻿using BankAccount.Domain.Account;
using System;
using System.Collections.Generic;

namespace BankAccount.Domain.Customer
{
    public class Customer
    {
        public Customer(string firstName, string lastName)
        {

            if (string.IsNullOrEmpty(firstName))
            {
                throw new ArgumentNullException(nameof(firstName));
            }

            if (string.IsNullOrEmpty(lastName))
            {
                throw new ArgumentNullException(nameof(lastName));
            }


            IdCustomer = Guid.NewGuid();
            FirstName = firstName;
            LastName = lastName;
            _accounts = new AccountCollectionDatas();
        }

        public Guid IdCustomer { get; private set; }
        public string FirstName { get; private set; }
        public string LastName { get; private set; }


        public IReadOnlyCollection<Guid> Accounts
        {
            get
            {
                IReadOnlyCollection<Guid> readOnly = _accounts.GetAccountIds();
                return readOnly;
            }
        }

        private AccountCollectionDatas _accounts { get;  set; }

        public void Register(Guid accountId)
        {
            _accounts.AddAccount(accountId);
        }

    }
}