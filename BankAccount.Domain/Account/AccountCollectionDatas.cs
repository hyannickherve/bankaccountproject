﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace BankAccount.Domain.Account
{
    public class AccountCollectionDatas
    {
        private readonly IList<Guid> _listAccount;

        public AccountCollectionDatas()
        {

            _listAccount = new List<Guid>();
        }

        public void AddAccount(Guid accountId)
        {
            _listAccount.Add(accountId);
        }

        public IReadOnlyCollection<Guid> GetAccountIds()
        {
            IReadOnlyCollection<Guid> accountIds = new ReadOnlyCollection<Guid>(_listAccount);
            return accountIds;
        }
    }
}
