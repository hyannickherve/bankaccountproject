﻿using BankAccount.Domain.Common;
using System;
using System.Collections.Generic;

namespace BankAccount.Domain.Account
{
    public sealed class BankAccount
    {
        public Guid guid;

        public BankAccount()
        {
            _listOperations = new OperationCollection();
            guid = Guid.NewGuid();
        }


        public IReadOnlyCollection<IOperation> GetListOperations()
        {
            return _listOperations.GetListOperations();

        }

        private OperationCollection _listOperations;

        //balance TODO


        public BankAccount(Guid idcustomer)
        {
            this.guid = Guid.NewGuid(); 
            IdCustomer = idcustomer;
            _listOperations = new OperationCollection();

        }

        public Guid IdCustomer { get; set; }

        public void Deposit(Amount montant)
        {
            Credit credit = new Credit(guid, montant);
            _listOperations.AddOperation(credit);
        }

        public void WithDraw(Amount montant)
        {
            if (_listOperations.GetBalance() < montant)
                throw new InsuficientFundsException($"The account {guid} does not have enough funds to withdraw {montant}.");

            Debit debit = new Debit(guid, montant);
            _listOperations.AddOperation(debit);
        }

        public Amount GetBalance()
        {
            return _listOperations.GetBalance();
        }

        public IOperation GetLastOperation()
        {
            return _listOperations.GetLastOperation();
        }


        public void Close()
        {
            if (_listOperations.GetBalance() > 0)
                throw new BankAccountCloseException($"Le compte {guid} ne peut être fermé: il est encore crédité.");
        }

        public static BankAccount SetBankAccountDatas(Guid id, Guid idCustomer, List<IOperation> listOperations)
        {
            BankAccount account = new();


            return account;
        }
    }
}
