﻿using BankAccount.Domain.Common;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankAccount.Domain.Account
{
    public sealed class OperationCollection
    {
        private readonly IList<IOperation> _listOperations;

        public OperationCollection()
        {
            _listOperations = new List<IOperation>();
        }

        public IReadOnlyCollection<IOperation> GetListOperations()
        {
            return new ReadOnlyCollection<IOperation>(_listOperations);
        }

        public Amount GetBalance()
        {
            Amount totalValue = 0;

            foreach (IOperation item in _listOperations)
            {
                if (item is Debit)
                    totalValue -= item.Montant;

                if (item is Credit)
                    totalValue += item.Montant;
            }

            return totalValue;
        }

        public void AddOperation(IOperation credit)
        {
            _listOperations.Add(credit);
        }


        public IOperation GetLastOperation()
        {
            return _listOperations[_listOperations.Count - 1];
        }

    }
}
