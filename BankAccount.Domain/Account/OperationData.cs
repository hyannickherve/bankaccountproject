﻿using System;

namespace BankAccount.Domain.Account
{
    public class OperationData
    {

        public double Amount { get; }
      
        public DateTime OperationDate
        {
            get;
        }


        public OperationData(double amount, DateTime operationDate)
        {
            Amount = amount;
            OperationDate = OperationDate;
        }
    }
}